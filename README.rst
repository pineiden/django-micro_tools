========
Django Micro Tools
========

These are some generic tools to use on the Django apps

Quick Start
-----------

1.- For 'context' tools add on the settings file the variables definition. Then add to the context proccessors
2.- For 'currency' install the python-forex app 
3.- Use the 'get_env' to obtain the environmental variables