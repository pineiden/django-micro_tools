import pycountry
from forex_python.converter import CurrencyCodes

def get_currencies_list():
	CH_LIST=[]
	cc = CurrencyCodes()
	currencies=pycountry.currencies
	for c in currencies:
		code=c.alpha_3
		symbol=cc.get_symbol(code)
		pair=(code, symbol)
		if symbol:
			CH_LIST.append( pair)		
	return CH_LIST
