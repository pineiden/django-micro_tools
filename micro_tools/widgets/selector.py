from datetime import datetime
from django import forms
from django.template.loader import render_to_string



class AutoCompleteSearchWidget(forms.widgets.Input):
    def render(self, name, value, attrs=None):
        #print(self.attrs)
        data = {
	        'attrs': self.attrs, 
	        'name': name,
	        'selection': self.attrs["selection"],
	        'default_value':self.attrs["default_value"]}
        return render_to_string(
        	'widgets/selector/_autocomplete_search_widget.html', data)

