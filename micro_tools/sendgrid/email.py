import sendgrid
import os
from sendgrid.helpers.mail import *

from django.conf import settings
from django import template
from django.template.loader import get_template

import ujson as json


class SendMail:
    def __init__(self, *args, **kwargs):
        #essage = sendgrid.Mail()
        email = Mail()
        personalization = Personalization()
        # set from email
        ORIGIN_EMAIL = settings.ORIGIN_EMAIL

        from_email = Email(email=ORIGIN_EMAIL, name=settings.SITE_NAME)

        print(from_email.name)
        print(from_email)

        email.from_email = from_email
        # set to email

        to_email = object
        if 'user_name' in kwargs.keys():
            to_email = Email(
                email=kwargs['user_email'], name=kwargs['user_name'])
        else:
            to_email = Email(email=kwargs['user_email'])

        personalization.add_to(to_email)
        # SET subject
        email.subject = kwargs['subject']
        # HTML META
        personalization.add_header(Header("X-Test", "test"))
        personalization.add_header(Header("X-Mock", "true"))

        if 'content' in kwargs.keys():
            content = Content("text/plain", kwargs['content'])
        elif 'substitution' in kwargs.keys():
            substitution = kwargs["substitution"]
            for key, value in substitution:
                personalization.add_substitution(
                    Substitution(key, value))

        if 'filter' in kwargs.keys():
            filters = kwargs['filters']
            for app, contents in filters.items():
                for setting, value in contents['settings'].items():
                    personalization.add_filter(app, setting, value)

        email.add_personalization(personalization)

        template_result = ''

        if 'template_name' and 'context' in kwargs.keys():
            template_name = kwargs['template_name']
            data_context = kwargs['context']
            template = get_template(template_name)
            template_result = template.render(data_context)
            # print(template_result)
            # print(Content(template_result))
            email.add_content(Content("text/html", template_result))

        self.email = email
        APIKEY = settings.SENDGRID
        # print(APIKEY)
        self.sg = sendgrid.SendGridAPIClient(apikey=APIKEY)
        self.response = object

    def send(self):
        self.response = self.sg.client.mail.send.post(
            request_body=self.email.get())

    def status(self):
        return self.response.body

    def client(self):
        return self.sg

    def get_mail(self):
        return self.email

    def client_send(self, xmail):
        self.sg.client.mail.send.post(request_body=xmail.get())
