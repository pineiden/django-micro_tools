from django.core.exceptions import ValidationError

from django.utils.deconstruct import deconstructible

@deconstructible
class FileSize:
	def __init__(self, *args,**kwargs):
		lower_limit=kwargs['lower_limit']
		higher_limit=kwargs['higher_limit']

	def __call__(self, value):
		try:
			file_size=value._size
			if self.lower_limit>file_size: 
				raise ValidationError('%s has low size to this requeriments %d bytes' % (value, self.lower_limit))
			elif self.higher_limit<file_size:
				raise ValidationError('%s has more size that the requeriments %d bytes' % (value, self.higher_limit))
		except AttributeError as e:
			raise ValidationError('This value could not be validated for file type' % value)

	def __eq__(self, other):	
		file_size=other._size
		assert(file_size>self.lower_limit and self.higher_limit)