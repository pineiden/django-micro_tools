import os
from micro_tools.sendmail.email import SimpleMail
from datetime import datetime

send_email = {
}
fields = ('subject', 'body', 'from', 'to')
user_kwargs = dict(zip(
    fields,
    ('¡Inscripción a Curso de Programación realizada!',
     "La inscripcion al curso ha sido exitosa, muchas gracias.",
     'cursoprogramacion@disroot.com',
     [cd.get('email', 'dpineda@uchile.cl')],)))
user_kwargs.update({
    'nivel_nombre': 'Procesamiento de la Información con la terminal de GnuLinux',
    'nombre_completo': "David Pineda Osorio",
    'fecha_inicio': datetime.now()
})

mail.send()
