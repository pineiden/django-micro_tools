import os
import ujson as json

from django.conf import settings
from django.template.loader import render_to_string


from django.core.mail import send_mail

"""
send_mail("Subject", "text body", "from@example.com",
          ["to@example.com"], html_message="<html>html body</html>")
"""


class SimpleMail:
    __fields = ('subject', 'body', 'from', 'to')

    def __init__(self, *args, **kwargs):
        self._args = [kwargs.get(k) for k in self.__fields]
        self._template = kwargs.get('template_path', None)
        self._tpl_info = kwargs.get('template_info', {})
        self._debug = kwargs.get('debug', True)

    def send(self):
        body = self.load_template()
        send_mail(*self._args,
                  html_message=body,
                  fail_silently=not self._debug)

    def load_template(self):
        return render_to_string(self._template, context=self._tpl_info)
